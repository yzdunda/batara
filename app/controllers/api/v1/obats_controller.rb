class Api::V1::ObatsController < ApplicationController

	# GET /obats
	def index
		@obats = Obat.all
		render json: @obats
	end

	# GET /obats/n
	def show  
		@obat = Obat.find(params[:id])
		render json: @obat
	end

	# POST /obats
	def create
		@obat = Obat.new(obat_params)
		if @obat.save
			render json: @obat, status: :created, location: @obat
		else
			render json: @obat.error, status: :unprocessable_entity
		end
	end

	# PUT/PATCH /Obats/n
	def update
		@obat = Obat.find(params[:id])
		if @obat.update(obat_params)
			render json: @obat
		else
			render json: @obat.error, status: :unprocessable_entity
		end
	end

	# DELETE /obats/n
	def destroy
		@obat = Obat.find(params[:id])
		@obat.destroy
	end


	private

	def obat_params
		params.require(:obat).permit(:nama_obat, :kode_obat, :dosis, :jumlah)
	end

end
