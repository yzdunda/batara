class Api::V1::DoktersController < ApplicationController
	# api for dokter
	# GET /dokters
	def index
		@dokters = Dokter.all
		render json: @dokters		
	end

	# GET /dokters/n
	def show
		@dokter = Dokter.find(params[:id])
		render json: @dokter
	end

	# POST /dokters
	def create
		@dokter = Dokter.new(dokter_params)
		if @dokter.save
			render json: @dokter, status: :created, location: @dokter
		else
			render json: @dokter.error, status: :unprocessable_entity
		end		
	end

	# PATCH/PUT dokters/n
	def update
		@dokter = Dokter.find(params[:id])
		if @dokter.update(dokter_params)
			render json: @dokter
		else
			render json: @dokter.error, status: :unprocessable_entity
		end
	end

	# DELETE /dokters/n
	def destroy
		@dokter = Dokter.find(params[:id])
		@dokter.destroy
	end

	private 

	def dokter_params 
		params.require(:dokter).permit(:id_dokter, :status, :nama_dokter, :nama_puskesmas, :jadwal_dokter)
	end

end
