class Api::V1::PuskesmasController < ApplicationController

	# GET /puskesmas
	def index
		@puskesmas = Puskesma.all
		render json: @puskesmas
	end

	#GET /puskesmas/n
	def show
		@puskesma = Puskesma.find(params[:id])
		render json: @puskesma
	end

	# POST /puskesmas
	def create
		@puskesma = Puskesma.new(puskesma_params)
		if @puskesma.save
			render json: @puskesma, status: :created, location: @puskesma
		else
			render json: @puskesma.error, status: :unprocessable_entity
		end
	end

	# PATCH/PUT puskesmas/n
	def update
		@puskesma = Puskesma.find(params[:id])
		if @puskesma.update(puskesma_params)
			render json: @puskesma
		else
			render json: @puskesma.error, status: :unprocessable_entity
		end
	end


	# DELETE /puskesmas
	def destroy
		@puskesma = Puskesma.find(params[:id])
		@puskesma.destroy
	end


	private 

	def puskesma_params 
		params.require(:puskesma).permit(:id_puskesmas, :nama_puskesmas, :alamat_puskesmas, :kontak, :penanggung_jawab)
	end

end
