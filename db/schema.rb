# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180418194731) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "diagnosas", force: :cascade do |t|
    t.string "nama_poli"
    t.string "nama_dokter"
    t.text "diagnosa"
    t.bigint "pasiens_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pasiens_id"], name: "index_diagnosas_on_pasiens_id"
  end

  create_table "dokters", force: :cascade do |t|
    t.integer "id_dokter"
    t.boolean "status"
    t.string "nama_dokter"
    t.boolean "jenis_kelamin"
    t.string "nama_puskesmas"
    t.string "nama_poli"
    t.date "jadwal_dokter"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "rabu"
    t.boolean "kamis"
    t.boolean "jumat"
    t.boolean "sabtu"
    t.boolean "minggu"
    t.boolean "senin"
    t.boolean "selasa"
  end

  create_table "obats", force: :cascade do |t|
    t.string "nama_obat"
    t.string "kode_obat"
    t.string "dosis"
    t.string "jumlah"
    t.bigint "pasiens_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pasiens_id"], name: "index_obats_on_pasiens_id"
  end

  create_table "pasiens", force: :cascade do |t|
    t.string "id_ktp"
    t.string "nama_pasien"
    t.string "alamat_pasien"
    t.boolean "jenis_kelamin"
    t.string "tempat_lahir"
    t.date "tanggal_lahir"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "status_login"
    t.index ["id_ktp"], name: "index_pasiens_on_id_ktp", unique: true
  end

  create_table "puskesmas", force: :cascade do |t|
    t.integer "id_puskesmas"
    t.string "nama_puskesmas"
    t.string "alamat_puskesmas"
    t.string "kontak"
    t.string "penanggung_jawab"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["id_puskesmas"], name: "index_puskesmas_on_id_puskesmas", unique: true
  end

  create_table "rekam_medis", force: :cascade do |t|
    t.string "nik"
    t.string "nama_pasien"
    t.string "nama_poli"
    t.string "nama_dokter"
    t.string "obat"
    t.text "diagnosa"
    t.bigint "pasien_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["nik"], name: "index_rekam_medis_on_nik", unique: true
    t.index ["pasien_id"], name: "index_rekam_medis_on_pasien_id"
  end

  create_table "tanda_vitals", force: :cascade do |t|
    t.string "tekanan_darah"
    t.string "nadi"
    t.string "frekuensi_pernapasan"
    t.float "suhu_tubuh"
    t.integer "tinggi_badan"
    t.integer "berat_badan"
    t.string "golongan_darah"
    t.boolean "resus"
    t.bigint "pasien_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pasien_id"], name: "index_tanda_vitals_on_pasien_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "diagnosas", "pasiens", column: "pasiens_id"
  add_foreign_key "obats", "pasiens", column: "pasiens_id"
  add_foreign_key "rekam_medis", "pasiens"
end
