class CreateObats < ActiveRecord::Migration[5.1]
  def change
    create_table :obats do |t|
      t.string :nama_obat
      t.string :kode_obat
      t.string :dosis
      t.string :jumlah
      t.references :pasiens, foreign_key: true

      t.timestamps
    end
  end
end
