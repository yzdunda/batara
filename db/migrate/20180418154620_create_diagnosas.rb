class CreateDiagnosas < ActiveRecord::Migration[5.1]
  def change
    create_table :diagnosas do |t|
      t.string :nama_poli
      t.string :nama_dokter
      t.text :diagnosa
      t.references :pasiens, foreign_key: true
      
      t.timestamps
    end
  end
end
