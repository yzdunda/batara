class CreateDokters < ActiveRecord::Migration[5.1]
  def change
    create_table :dokters do |t|
      t.integer :id_dokter
      t.boolean :status
      t.string :nama_dokter
      t.boolean :jenis_kelamin
      t.string :nama_puskesmas
      t.string :nama_poli
      t.date :jadwal_dokter

      t.timestamps
    end
  end
end
