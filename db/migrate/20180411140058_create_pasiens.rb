class CreatePasiens < ActiveRecord::Migration[5.1]
  def change
    create_table :pasiens do |t|
      t.string :id_ktp
      t.string :nama_pasien
      t.string :alamat_pasien
      t.boolean :jenis_kelamin
      t.string :tempat_lahir
      t.date :tanggal_lahir

      t.timestamps
    end
    add_index :pasiens, :id_ktp, unique: true
  end
end
