class CreateRekamMedis < ActiveRecord::Migration[5.1]
  def change
    create_table :rekam_medis do |t|
      t.string :nik
      t.string :nama_pasien
      t.string :nama_poli
      t.string :nama_dokter
      t.string :obat
      t.text :diagnosa
      t.references :pasien, foreign_key: true

      t.timestamps
    end
    add_index :rekam_medis, :nik, unique: true
  end
end
