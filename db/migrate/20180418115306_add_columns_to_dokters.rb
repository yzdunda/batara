class AddColumnsToDokters < ActiveRecord::Migration[5.1]
  def change
    add_column :dokters, :rabu, :boolean
    add_column :dokters, :kamis, :boolean
    add_column :dokters, :jumat, :boolean
    add_column :dokters, :sabtu, :boolean
    add_column :dokters, :minggu, :boolean
  end
end
