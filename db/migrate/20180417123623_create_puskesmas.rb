class CreatePuskesmas < ActiveRecord::Migration[5.1]
  def change
    create_table :puskesmas do |t|
      t.integer :id_puskesmas
      t.string :nama_puskesmas
      t.string :alamat_puskesmas
      t.string :kontak
      t.string :penanggung_jawab

      t.timestamps
    end
    add_index :puskesmas, :id_puskesmas, unique: true
  end
end
