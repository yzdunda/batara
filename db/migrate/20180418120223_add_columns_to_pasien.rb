class AddColumnsToPasien < ActiveRecord::Migration[5.1]
  def change
    add_column :pasiens, :rabu, :boolean
    add_column :pasiens, :kamis, :boolean
    add_column :pasiens, :jumat, :boolean
    add_column :pasiens, :sabtu, :boolean
    add_column :pasiens, :minggu, :boolean
  end
end
