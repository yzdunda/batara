class RemoveColumnsToPasien < ActiveRecord::Migration[5.1]
  def change
    remove_column :pasiens, :rabu, :boolean
    remove_column :pasiens, :kamis, :boolean
    remove_column :pasiens, :jumat, :boolean
    remove_column :pasiens, :sabtu, :boolean
    remove_column :pasiens, :minggu, :boolean
  end
end
